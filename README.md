# Moonlander-Trackpoint-AddOn

This project contains stl and freecad-files for a Trackpoint to be added to a Moonlander. I liked the Moonlander very much, but switching my right hand between keyboard and mouse annoyed me. I designed this AddOn to have a full-featured mouse on my Keyboard. 

## Hardware

You will need an old Trackpoint module from a Thinkpad. Usually they speak PS2 and there are some pinouts out there on the Internet. I connected 3 buttons to the Mouse-button solder pads and extended them via RJ45 connectors to the left side of the Keyboard. The contacts for the PS2 connecection to the PC are extended to the left side via the same cable and they connect to a generic PS2 to USB adapter. No drivers are needed to use this mouse.

## Problems and planned improvements

The only problem I encountered is that if you disconnect the computer from the ground, you have to re-insert the usb-adapter a few times before it starts working again. I dont know wether this is an issue with the PS2 to USB adapter or a grounding issue with the Trackpoint module.

I plan to improve the wiring below the keyboard to be enclosed. This way the Moonlander fits in the case again. After I did that I may message ZSA wether they want to add my mod to their [site](https://www.zsa.io/moonlander/printables/).
